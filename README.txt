Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html


~= Server configuration =~

Launch ServerScript.exe and then go to Apache24\bin\httpd.exe and launch the server

Default port is set to 8080.
Default URL in browser is: http://localhost:8080/


~= Manual Server Configuration =~

In Apache24\conf\httpd.conf change:
37: Server root to folder path
237,238: DocumentRoot to path where http documents are
528: path to PHP directory
532: path to PHP directory


In PHP\php.ini
730: path to php "ext" extention folder