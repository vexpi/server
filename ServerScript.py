#encoding=utf-8
'''
Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html'''
import sys, os


if __name__ == '__main__':
    print "Starting script"
    try:
        pathname = os.path.dirname(sys.argv[0])
         # testausta varten
        exePath = "D:\\programming\\vexpi 1.0\\Server"
        
        userInput = raw_input("Is this the path for your server directory ?( Where Apache24 is located in)\n "+pathname+" (y/n):")
        if userInput == "n":
            inputPath = raw_input("Give me the path then:")
            pathname = inputPath
        
        
        pathString = str(pathname)
        pathString = pathString.replace("\\ServerScript\\dist\\ServerScript", "")
        pathx = pathString.replace("\\", "/")
        
        f = open(pathString +'\Apache24\conf\httpd.conf', 'r')    # pass an appropriate path of the required file
        lines = f.readlines()
        f.close()
        fc = open(pathString + '\Apache24\conf\httpd.conf', 'w') 
        
        
        lines[36] = "ServerRoot \"" + pathx + "/Apache24\"\n"   # n is the line number you want to edit; subtract 1 as indexing of list starts from 0
        print lines[36]
        lines[236] = "DocumentRoot \""+ pathx +"/htdocs\"\n"
        print lines[236]
        lines[237] = "<Directory \""+ pathx +"/htdocs\">\n"
        print lines[237]
        lines[527] = "LoadModule php5_module \""+ pathx +"/PHP/php5apache2_4.dll\"\n"
        print lines[527]
        lines[531] = "PHPIniDir \""+ pathx +"/PHP\"\n"
        print lines[531]
       
        fc.writelines(lines)
        fc.close()
        
        f1 = open(pathString +'\\PHP\\php.ini', 'r')    # pass an appropriate path of the required file
        lines = f1.readlines()
        f1.close()
        fc2 = open(pathString +'\\PHP\\php.ini', 'w') 
        
        
        lines[729] = "extension_dir = \""+pathString+"\\PHP\\ext\"\n"
        print lines[729]
    
       
        fc2.writelines(lines)
        fc2.close()
        print "===================================================="
        print "Everything went well, starting Vexpi-server"
        print "Server is running! (go to http://localhost:8080/)"
        
        os.system(pathString +'\\Apache24\\bin\\httpd.exe')
        
        
    except Exception, e:
        print "Something went wrong :(" 