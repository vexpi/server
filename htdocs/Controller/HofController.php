<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->
<?php

include $_SERVER['DOCUMENT_ROOT'].'/Model/HofStat.php';

function getView($parameter)
{
	$stat = isset($_GET['stat']) ? $_GET["stat"] :  '';	
	
$title = '';
$tabletitle = '';

switch($stat){
case 'Jumax':
$activeButton = 2;
$title = "Junan suurin myöhästyminen";
$tabletitle = 'Juna';
$stats = getTrainLateness('latenessMax');
break;
case 'Asavg':
$activeButton = 3;
$title = "Epätäsmällisin asema avg";
$tabletitle = 'Asema';
$stats = getStationLateness("latenessAverage");
break;

case 'Asmax':
$title = "Epätäsmällisin asema";
$activeButton = 4;
$tabletitle = 'Asema';
$stats  = getStationLateness("latenessMax");
break;	
	
default:
$title = "Juna myöhässä avg";
$activeButton = 1;
$tabletitle = 'Juna';
$stats = getTrainLateness('latenessAverage');
				
	
}
include $_SERVER['DOCUMENT_ROOT'].'/View/hofView.php';			
}
		

?>
