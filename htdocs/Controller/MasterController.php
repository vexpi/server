<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->
<script>
			  $(document).ready(function(){			
					$('.adddateparams').find("a").click(function(event){
						event.preventDefault();
						var number = $('#number').attr('value'); 
						var year = $('#year').attr('value');
						var time = $("#time").find("option:selected").attr('value') ;
						stringToSend = "&number="+number+"&year="+year+"&time="+time;
						window.location.href = $(this).attr('href') + stringToSend;
					});
				}); 
				
			  </script>
<?php

include $_SERVER['DOCUMENT_ROOT'].'/Model/Stats.php';


function getView($parameter)
{
	
	if($parameter == "tracks")
	{
		// do something with models
		include $_SERVER['DOCUMENT_ROOT'].'/View/tracksView.php'; 
		// give parameters to function in view
	}
	else if($parameter == "top")
	{
		// do something with models
		include $_SERVER['DOCUMENT_ROOT'].'/View/topView.php'; 
		// give parameters to function in view
	}
	
	
	else
	{
		include $_SERVER['DOCUMENT_ROOT'].'/View/welcomeView.php'; 
	}
}
 ?>