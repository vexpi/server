<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->
<?php

include $_SERVER['DOCUMENT_ROOT'].'/Model/Stats.php';


function getView($parameter)
{
	

	if($parameter == "station")
	{
		$station = isset($_GET['station']) ? $_GET["station"] :  '';
		$stationcd = getStationCodeByName($station);
		$tracksar = getTracksToStation($stationcd);
		$tracks = getTracksFromStation($stationcd);
		
		// check date selection 
		$time = isset($_GET['time']) ? $_GET["time"] :  '';
		$number = isset($_GET['number']) ? $_GET["number"] :  '';
		$year = isset($_GET['year']) ? $_GET["year"] :  '';
		
		$statsar = array();
		$stats = array();
		switch($time)
		{
			case "month":
				foreach($tracks as $key)
				{
					$stats[] = getStats_month(Stats::all, $key,  $number, $year);
				}
				foreach($tracksar as $key)
				{
					$statsar[] = getStats_month(Stats::all, $key,  $number, $year);
				}
				break;
			case "week":
				foreach($tracks as $key)
				{
					$stats[] = getStats_week(Stats::all,$key, $number, $year);
				}
				foreach($tracksar as $key)
				{
					$statsar[] = getStats_week(Stats::all,$key, $number, $year);
				}
				break;
			case "recent":
				foreach($tracks as $key)
				{
					$stats[] = getStats_recent(Stats::all,$key);
				}
				foreach($tracksar as $key)
				{
					$statsar[] = getStats_recent(Stats::all,$key);
				}
				break;
			default:
				foreach($tracks as $key)
				{
					$stats[] = getStats_alltime(Stats::all,$key);
				}
				foreach($tracksar as $key)
				{
					$statsar[] = getStats_alltime(Stats::all,$key);
				}
				break;
		}
		
		include $_SERVER['DOCUMENT_ROOT'].'/View/stationView.php'; 
		// give parameters to function in view
	}
	else 
	{
		$time = isset($_GET['time']) ? $_GET["time"] :  '';
		$number = isset($_GET['number']) ? $_GET["number"] :  '';
		$year = isset($_GET['year']) ? $_GET["year"] :  '';
		$stats = getAllStationStats($time, $number, $year);
		include $_SERVER['DOCUMENT_ROOT'].'/View/stationsView.php';
	}
	//include $_SERVER['DOCUMENT_ROOT'].'/View/tracksView.php'; 
}
 ?>