<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->
<?php

include $_SERVER['DOCUMENT_ROOT'].'/Model/Stats.php';


function getView($parameter)
{
	if($parameter == "train")
	{
	
		// check date selection 
		$time = isset($_GET['time']) ? $_GET["time"] :  '';
		$number = isset($_GET['number']) ? $_GET["number"] :  '';
		$year = isset($_GET['year']) ? $_GET["year"] :  '';
		$guid = isset($_GET['guid']) ? $_GET["guid"] :  '';
		
		
		switch($time)
		{
			case "month":
				$train = getStats_month_train($guid, $number, $year);
				break;
			case "week":
				$train = getStats_week_train($guid, $number, $year);
				break;
			case "recent":
				$train = getStats_recent_train($guid);
				break;
			default:
				$train = getStats_alltime_train($guid);
		}
		
		include $_SERVER['DOCUMENT_ROOT'].'/View/trainView.php'; 
		// give parameters to function in view
	}
	else
	{
		
		$trainCat = isset($_GET['cat']) ? $_GET["cat"] :  '';
		
		// check date selection 
		$time = isset($_GET['time']) ? $_GET["time"] :  '';
		$number = isset($_GET['number']) ? $_GET["number"] :  '';
		$year = isset($_GET['year']) ? $_GET["year"] :  '';
		
		$stats = getAllTrainStats($trainCat, $time, $number, $year);
		
		include $_SERVER['DOCUMENT_ROOT'].'/View/trainsView.php';
		// give parameters to functions in view
	}
	
}
 ?>