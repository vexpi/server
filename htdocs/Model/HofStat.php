<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->
<?php
class Stats
{
	const all = 'All';
	public $guid; // if stats are for all trains on track $track, then $guid==all
	public $track; // if stats are for all tracks for train $train, then $track=all
	public $previousStation;
	public $nextStation;
	public $latenessAverage;
	public $latenessMax;
	public $speedAverage;
	public $speedMax;
	
	
	function __construct($guid, $track, $previousStation, $nextStation, $latenessAverage, $latenessMax, $speedAverage, $speedMax)
	{
		$this->guid=$guid;
		$this->track=$track;
		$this->previousStation=$previousStation;
		$this->nextStation=$nextStation;
		$this->latenessAverage=$latenessAverage;
		$this->latenessMax=$latenessMax;
		$this->speedAverage=$speedAverage;
		$this->speedMax=$speedMax;
		
	}
}



function getTrainLateness($searchKey)
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT t.* FROM ( SELECT * FROM Analyzeddata_alltime ORDER BY ". $searchKey ." DESC) 
t GROUP BY t.guid ORDER BY t.". $searchKey ." DESC LIMIT 20;");
	$trains = createStatsArrayGetStations($result, $con);
	mysqli_close($con);
	return $trains;
}


function getStationLateness($searchKey)
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT t.* FROM ( SELECT * FROM Analyzeddata_alltime_station ORDER BY ". $searchKey . " DESC) t GROUP BY t.station ORDER BY t.".$searchKey." DESC LIMIT 20;");
	$stations = createStationArray($result);
	mysqli_close($con);
	return $stations;
}


function createStationArray($result)
{
	$statsArray = array();
	while($row = mysqli_fetch_array($result))
	{
			$statsObj = new Stats($row['stationName'], 0, 0, 0, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		$statsArray[] = $statsObj;
	}
	return $statsArray;
}





function connectToDatabase()
{
	// Start connection
	$con = mysqli_connect('localhost:3306','vexpi','vexpi','vexpi');
	// Check connection
	if (mysqli_connect_errno($con))
	{
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	return $con;
}



function createStatsArray($result, $previousStation, $endStation)
{
	$statsArray = array();
	while($row = mysqli_fetch_array($result))
	{
		$statsObj = new Stats($row['guid'], $row['track'], $previousStation, $nextStation, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		$statsArray[] = $statsObj;
	}
	return $statsArray;
}
function createStatsArrayGetStations($result, $con)
{
	$statsArray = array();
	while($row = mysqli_fetch_array($result))
	{
		list($previousStation,$nextStation) = getTrackStationNames($con, $row['track']);
		$statsObj = new Stats($row['guid'], $row['track'], $previousStation, $nextStation, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		$statsArray[] = $statsObj;
	}
	return $statsArray;
}

function getTrackStationNames($con, $track)
{
	$result = mysqli_query($con,"SELECT * FROM track_fullnames WHERE idTrack=" . $track);
	$trackdata = mysqli_fetch_array($result);
	$previousStation = $trackdata['startStation'];
	$nextStation = $trackdata['endStation'];
	return array($previousStation, $nextStation);
}

function getTracksFromStation($startStation)
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT * FROM track WHERE startStation='" . $startStation . "'");
	
	$tracks = array();
	while($row = mysqli_fetch_array($result))
	{
		$tracks[] = $row['idTrack'];
	}
	mysqli_close($con);
	return $tracks;
}
function getTracksToStation($endStation)
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT * FROM track WHERE endStation='" . $endStation . "'");
	
	$tracks = array();
	while($row = mysqli_fetch_array($result))
	{
		$tracks[] = $row['idTrack'];
	}
	mysqli_close($con);
	return $tracks;
}
function getStationCodeByName($stationName)
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT * FROM station WHERE stationName='" . $stationName . "'");
	$row = mysqli_fetch_array($result);
	mysqli_close($con);
	return $row['stationCode'];
}


/*
echo 'koe1';
$station = getStationLateness('latenessMax');
echo 'koe2';
*/





?>
