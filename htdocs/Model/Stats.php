<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->
<?php
class Stats
{
	const all = 'All';
	public $guid; // if stats are for all trains on track $track, then $guid==all
	public $track; // if stats are for all tracks for train $train, then $track=all
	public $previousStation;
	public $nextStation;
	public $latenessAverage;
	public $latenessMax;
	public $speedAverage;
	public $speedMax;
	
	function __construct($guid, $track, $previousStation, $nextStation, $latenessAverage, $latenessMax, $speedAverage, $speedMax)
	{
		$this->guid=$guid;
		$this->track=$track;
		$this->previousStation=$previousStation;
		$this->nextStation=$nextStation;
		$this->latenessAverage=$latenessAverage;
		$this->latenessMax=$latenessMax;
		$this->speedAverage=$speedAverage;
		$this->speedMax=$speedMax;
	}
}

function getTrains($category)
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT * FROM train WHERE guid LIKE '" . $category . "%'");
	
	$trains = array();
	while($row = mysqli_fetch_array($result))
	{
		$trains[] = $row['guid'];
	}
	mysqli_close($con);
	return $trains;
}
function getTracks()
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT * FROM track");
	
	$tracks = array();
	while($row = mysqli_fetch_array($result))
	{
		$tracks[] = $row['idTrack'];
	}
	mysqli_close($con);
	return $tracks;
}
function getTracksFromStation($startStation)
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT * FROM track WHERE startStation='" . $startStation . "'");
	
	$tracks = array();
	while($row = mysqli_fetch_array($result))
	{
		$tracks[] = $row['idTrack'];
	}
	mysqli_close($con);
	return $tracks;
}
function getTracksToStation($endStation)
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT * FROM track WHERE endStation='" . $endStation . "'");
	
	$tracks = array();
	while($row = mysqli_fetch_array($result))
	{
		$tracks[] = $row['idTrack'];
	}
	mysqli_close($con);
	return $tracks;
}
function getStationCodeByName($stationName)
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT * FROM station WHERE stationName='" . $stationName . "'");
	$row = mysqli_fetch_array($result);
	mysqli_close($con);
	return $row['stationCode'];
}

function connectToDatabase()
{
	// Start connection
	$con = mysqli_connect('localhost:3306','vexpi','vexpi','vexpi');
	// Check connection
	if (mysqli_connect_errno($con))
	{
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	return $con;
}

function createStationArray($result)
{
	$statsArray = array();
	while($row = mysqli_fetch_array($result))
	{
		$statsObj = new Stats($row['stationName'], 0, 0, 0, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		$statsArray[] = $statsObj;
	}
	return $statsArray;
}
function createStatsArray($result, $previousStation, $nextStation)
{
	$statsArray = array();
	while($row = mysqli_fetch_array($result))
	{
		$statsObj = new Stats($row['guid'], ISSET($row['track']) ? $row['track'] : Stats::all, $previousStation, $nextStation, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		$statsArray[] = $statsObj;
	}
	return $statsArray;
}
function createStatsArrayGetStations($result, $con)
{
	$statsArray = array();
	while($row = mysqli_fetch_array($result))
	{
		list($previousStation,$nextStation) = getTrackStationNames($con, $row['track']);
		$statsObj = new Stats($row['guid'], $row['track'], $previousStation, $nextStation, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		$statsArray[] = $statsObj;
	}
	return $statsArray;
}

function getTrackStationNames($con, $track)
{
	$result = mysqli_query($con,"SELECT * FROM track_fullnames WHERE idTrack=" . $track);
	$trackdata = mysqli_fetch_array($result);
	$previousStation = $trackdata['startStation'];
	$nextStation = $trackdata['endStation'];
	return array($previousStation, $nextStation);
}

function getStats_alltime_train($guid)
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT * FROM analyzeddata_alltime WHERE guid='" . $guid . "'");
	$statsArray = createStatsArrayGetStations($result, $con);
	mysqli_close($con);
	return $statsArray;
}
function getStats_month_train($guid, $month, $year)
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT * FROM analyzeddata_month WHERE guid='" . $guid . "' AND month=" . $month . " AND year=" . $year);
	$statsArray = createStatsArrayGetStations($result, $con);
	mysqli_close($con);
	return $statsArray;
}
function getStats_week_train($guid, $week, $year)
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT * FROM analyzeddata_week WHERE guid='" . $guid . "' AND week=" . $week . " AND year=" . $year);
	$statsArray = createStatsArrayGetStations($result, $con);
	mysqli_close($con);
	return $statsArray;
}
function getStats_recent_train($guid)
{
	$con = connectToDatabase();
	$result = mysqli_query($con,"SELECT * FROM analyzeddata_recent WHERE guid='" . $guid . "'");
	$statsArray = createStatsArrayGetStations($result, $con);
	mysqli_close($con);
	return $statsArray;
}
function getStats_alltime_track($track)
{
	// get station names for $track
	$con = connectToDatabase();
	list($previousStation,$nextStation) = getTrackStationNames($con, $track);
	// get stats by individual train on $track
	$result = mysqli_query($con,"SELECT * FROM analyzeddata_alltime WHERE track=" . $track);
	$statsArray = createStatsArray($result, $previousStation, $nextStation);
	mysqli_close($con);
	return $statsArray;
}
function getStats_month_track($track, $month, $year)
{
	// get station names for $track
	$con = connectToDatabase();
	list($previousStation,$nextStation) = getTrackStationNames($con, $track);
	// get stats by individual train on $track
	$result = mysqli_query($con,"SELECT * FROM analyzeddata_alltime WHERE track=" . $track . " AND month=" . $month . " AND year=" . $year);
	$statsArray = createStatsArray($result, $previousStation, $nextStation);
	mysqli_close($con);
	return $statsArray;
}
function getStats_week_track($track, $week, $year)
{
	// get station names for $track
	$con = connectToDatabase();
	list($previousStation,$nextStation) = getTrackStationNames($con, $track);
	// get stats by individual train on $track
	$result = mysqli_query($con,"SELECT * FROM analyzeddata_alltime WHERE track=" . $track . " AND week=" . $week . " AND year=" . $year);
	$statsArray = createStatsArray($result, $previousStation, $nextStation);
	mysqli_close($con);
	return $statsArray;
}
function getStats_recent_track($track)
{
	// get station names for $track
	$con = connectToDatabase();
	list($previousStation,$nextStation) = getTrackStationNames($con, $track);
	// get stats by individual train on $track
	$result = mysqli_query($con,"SELECT * FROM analyzeddata_alltime WHERE track=" . $track);
	$statsArray = createStatsArray($result, $previousStation, $nextStation);
	mysqli_close($con);
	return $statsArray;
}

function getStats_alltime($guid, $track)
{
	$con = connectToDatabase();
	$statsObj = null;
	if($guid == Stats::all)
	{
		if($track == Stats::all)
		{
			// get overall stats
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_alltime_overall");
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, '', '', $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		}
		else
		{
			// get station names for $track
			list($previousStation,$nextStation) = getTrackStationNames($con, $track);
		
			// get stats of all trains on $track
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_alltime_track WHERE idTrack=" . $track);
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, $previousStation, $nextStation, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		}
	}
	else
	{
		if($track == Stats::all)
		{
			// get stats of $guid on all tracks
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_alltime_train WHERE guid='" . $guid . "'");
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, '', '', $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		}
		else
		{
			// get station names for $track
			list($previousStation,$nextStation) = getTrackStationNames($con, $track);
			
			// get stats of $guid on $track
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_alltime WHERE guid='" . $guid . "' AND track=" . $track);
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, $previousStation, $nextStation, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
			
		}
	}
	mysqli_close($con);
	return $statsObj;
}
function getStats_month($guid, $track, $month, $year)
{
	$con = connectToDatabase();
	$statsObj = null;
	if($guid == Stats::all)
	{
		if($track == Stats::all)
		{
			// get overall stats
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_month_overall WHERE month=" . $month . " AND year=" . $year);
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, '', '', $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		}
		else
		{
			// get station names for $track
			list($previousStation,$nextStation) = getTrackStationNames($con, $track);
		
			// get stats of all trains on $track
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_month_track WHERE idTrack=" . $track . " AND month=" . $month . " AND year=" . $year);
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, $previousStation, $nextStation, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		}
	}
	else
	{
		if($track == Stats::all)
		{
			// get stats of $guid on all tracks
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_month_train WHERE guid='" . $guid . "' AND month=" . $month . " AND year=" . $year);
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, '', '', $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		}
		else
		{
			// get station names for $track
			list($previousStation,$nextStation) = getTrackStationNames($con, $track);
			
			// get stats of $guid on $track
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_month WHERE guid='" . $guid . "' AND track=" . $track . " AND month=" . $month . " AND year=" . $year);
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, $previousStation, $nextStation, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
			
		}
	}
	mysqli_close($con);
	return $statsObj;
}
function getStats_week($guid, $track, $week, $year)
{
	$con = connectToDatabase();
	$statsObj = null;
	if($guid == Stats::all)
	{
		if($track == Stats::all)
		{
			// get overall stats
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_week_overall WHERE week=" . $week . " AND year=" . $year);
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, '', '', $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		}
		else
		{
			// get station names for $track
			list($previousStation,$nextStation) = getTrackStationNames($con, $track);
		
			// get stats of all trains on $track
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_week_track WHERE idTrack=" . $track . " AND week=" . $week . " AND year=" . $year);
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, $previousStation, $nextStation, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		}
	}
	else
	{
		if($track == Stats::all)
		{
			// get stats of $guid on all tracks
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_week_train WHERE guid='" . $guid . "' AND week=" . $week . " AND year=" . $year);
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, '', '', $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		}
		else
		{
			// get station names for $track
			list($previousStation,$nextStation) = getTrackStationNames($con, $track);
			
			// get stats of $guid on $track
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_week WHERE guid='" . $guid . "' AND track=" . $track . " AND week=" . $week . " AND year=" . $year);
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, $previousStation, $nextStation, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
			
		}
	}
	mysqli_close($con);
	return $statsObj;
}
function getStats_recent($guid, $track)
{
	$con = connectToDatabase();
	$statsObj = null;
	if($guid == Stats::all)
	{
		if($track == Stats::all)
		{
			// get overall stats
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_recent_overall");
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, '', '', $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		}
		else
		{
			// get station names for $track
			list($previousStation,$nextStation) = getTrackStationNames($con, $track);
		
			// get stats of all trains on $track
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_recent_track WHERE idTrack=" . $track);
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, $previousStation, $nextStation, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		}
	}
	else
	{
		if($track == Stats::all)
		{
			// get stats of $guid on all tracks
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_recent_train WHERE guid='" . $guid . "'");
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, '', '', $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
		}
		else
		{
			// get station names for $track
			list($previousStation,$nextStation) = getTrackStationNames($con, $track);
			
			// get stats of $guid on $track
			$result = mysqli_query($con,"SELECT * FROM analyzeddata_recent WHERE guid='" . $guid . "' AND track=" . $track);
			$row = mysqli_fetch_array($result);
			$statsObj = new Stats($guid, $track, $previousStation, $nextStation, $row['latenessAverage'], $row['latenessMax'], $row['speedAverage'], $row['speedMax']);
			
		}
	}
	mysqli_close($con);
	return $statsObj;
}

function getAllTrainStats($category, $time, $number, $year)
{
	$db='';
	$addparams='';
	switch($time)
	{
		case "month":
			$db = 'analyzeddata_month_train';
			$addparams = ' AND month=' . $number . ' AND year=' . $year;
			break;
		case "week":
			$db = 'analyzeddata_week_train';
			$addparams = ' AND week=' . $number . ' AND year=' . $year;
			break;
		case "recent":
			$db = 'analyzeddata_recent_train';
			break;
		default:
			$db = 'analyzeddata_alltime_train';
			break;
	}
	$con = connectToDatabase();
	$statsObj = null;
	$result = mysqli_query($con,"SELECT * FROM " . $db . " a WHERE a.guid IN (SELECT * FROM train t WHERE t.guid LIKE '" . $category . "%')" . $addparams);
	$statsArray = createStatsArray($result, Stats::all, Stats::all);
	mysqli_close($con);
	return $statsArray;
}
function getAllStationStats($time, $number, $year)
{
	$db='';
	$addparams='';
	switch($time)
	{
		case "month":
			$db = 'analyzeddata_month_station';
			$addparams = ' WHERE month=' . $number . ' AND year=' . $year;
			break;
		case "week":
			$db = 'analyzeddata_week_station';
			$addparams = ' WHERE week=' . $number . ' AND year=' . $year;
			break;
		case "recent":
			$db = 'analyzeddata_recent_station';
			break;
		default:
			$db = 'analyzeddata_alltime_station';
			break;
	}
	$con = connectToDatabase();
	$statsObj = null;
	$result = mysqli_query($con,"SELECT * FROM " . $db . " a" . $addparams);
	$statsArray = createStationArray($result);
	mysqli_close($con);
	return $statsArray;
}
?>
