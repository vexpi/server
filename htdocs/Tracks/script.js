/*Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html */

var marker = new google.maps.Marker( );
var trainID = "H315" ;
var map ;
var ctaLayer;
function loadXMLDoc()
{
var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
		var items =xmlhttp.responseXML.documentElement.getElementsByTagName("item");
		for (i=0;i<items.length;i++)
		{
			var guid = items[i].getElementsByTagName("guid")[0].childNodes[0].nodeValue;
			alert(guid);
			if( guid = trainID)
			{
				var location = items[i].getElementsByTagName("georss:point")[0].childNodes[0].nodeValue;
				alert(location);
			}
		}
    }
  }
xmlhttp.open("GET","http://188.117.35.14/TrainRSS/TrainService.svc/AllTrains",true);
xmlhttp.send();
}

function initialize() {
var styles = [
  {
    stylers: [
      { hue: "#00ffe6" },
      { saturation: -20 }
    ]
  },{
    featureType: "transit.line",
    elementType: "geometry.fill",
    stylers: [
      { weight: 8 },
      { color: "#ff0000" }
    ]
  },{
    featureType: "road",
    stylers: [
      { visibility: "on" },
	  { saturation : -100}
    ]
  }
];

var mapOptions = {
  center: new google.maps.LatLng(62.2500, 25.7500),
  zoom: 6,
  disableDefaultUI: true,
  mapTypeId: google.maps.MapTypeId.ROADMAP
};
map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
var myLatLng = new google.maps.LatLng( 62.2500, 25.7500 );
marker.setMap(map);
marker.setPosition(myLatLng);
map.setCenter(new google.maps.LatLng(62.2500, 25.7500));
map.setOptions({styles: styles});
}
// url: 'http://users.jyu.fi/~eejohaka/mapdata.kml'
// url: 'http://users.jyu.fi/~jearsaju/bla.kml'
function moveMarker(  ) {  
	var lat = 61.8639;
	var lng = 25.7500;
	var newLatLng = new google.maps.LatLng( lat, lng );
    //delayed so you can see it move
    setTimeout( function(){     
        marker.setPosition( newLatLng );
    }, 10 );
};

function setKmlLayer() {
if(ctaLayer != null){ ctaLayer.setMap(null);}// map = null; initialize();}
ctaLayer = new google.maps.KmlLayer({
    url: $('#kmlUrl').attr('value'),
	preserveViewport : true
  });
  ctaLayer.setMap(map);
}

function moveMarkerTo( lat,lng ) {  
    //delayed so you can see it move
    setTimeout( function(){     
        marker.setPosition( new google.maps.LatLng( lat, lng ) );
    }, 10 );
};

function toNewLocation() {		
	var lat = $('#lat').attr('value'); 
	var lng = $('#lng').attr('value');
	moveMarkerTo(lat,lng);
};

function pinTrain(train){
	loadXMLDoc();
}

google.maps.event.addDomListener(window, 'load', initialize);
//setTimeout( pinTrain(trainID), 100 );
