<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->
<!DOCTYPE html>
<html lang="fi">
  <head>
	<?php include $_SERVER['DOCUMENT_ROOT'].'/meta.php' ?>
  </head>

  <body>

    <div class="container">

    <?php
	$active = 'trains';
	include $_SERVER['DOCUMENT_ROOT'].'/header.php' ?>
	
	<?php
	
	// tässä haetaan controllerin kautta uutta tietoa tälle sivulla
      include $_SERVER['DOCUMENT_ROOT'].'/Controller/TrainsController.php';
	  
	  //function customError($errno, $errstr)
	  //{
	  ////echo "<b>Error:</b> [$errno] $errstr<br>";
	  ////echo "Ending Script";
	  ////die();
	  //}
	  //set_error_handler("customError");  // poistaa kaikki varoitukset, asetetaan päälle kun on valmis ohjelma
	  getView(isset($_GET['page']) ? $_GET["page"] :  '');
	?>
	
	<?php include $_SERVER['DOCUMENT_ROOT'].'/footer.php'?>

  </body>
</html>
