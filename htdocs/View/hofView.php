<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->
	  <div class="jumbotron">
        		<h2>
		<?php echo $title;?>
		</h2>
		<p class="lead">
		<div class="navbar withoutMargin">
          <div class="navbar-inner">
            <div class="container">
			<!--
			<form method="post" class="navbar-form pull-center">
			
			<select id="time" name="time">
				<option value="all" <?php echo $time!="month" && $time!="week" && $time!="recent" ? "selected" : '' ?>>Kaikki</option>
				<option value="recent" <?php echo $time=="recent" ? "selected" : '' ?>>Viimeaikaiset</option>
				<option value="month" <?php echo $time=="month" ? "selected" : '' ?>>Kuukausi</option>
				<option value="week" <?php echo $time=="week" ? "selected" : '' ?>>Viikko</option>
			</select>
			
			<input type="text" id="number" name="number" value="<?php echo $number; ?>">
			<input type="text" id="year" name="year" value="<?php echo $year; ?>">
			</form>
			-->
              <ul class="nav">
				
				<li <?php if($activeButton == 1) echo 'class="active"'; ?>><a href="/Hof/?stat=Juavg">Juna myöhässä avg</a></li>
				<li <?php if($activeButton == 2) echo 'class="active"'; ?>><a href="/Hof/?stat=Jumax">Juna myöhässä max</a></li>
				<li <?php if($activeButton == 3) echo 'class="active"'; ?>><a href="/Hof/?stat=Asavg">Epätäsmällisin asema avg</a></li>
				<li <?php if($activeButton == 4) echo 'class="active"'; ?>><a href="/Hof/?stat=Asmax">Epätäsmällisin asema max</a></li>
				
 
              </ul>
            </div>
          </div>
        </div>
		       
		   <table border=1  class="table table-hover tabletrains tablesorter addSortparams">
			<thead>
			<tr class="tabletitlerow">
				<th>&#35; </th>
				<th><?php echo $tabletitle?></th>
				<th>Myöhässä avg</th>
				<th>Myöhässä max</th>
				<th>Keskinopeus</th>
				<th>Nopeus max</th>
			</tr>
			</thead>
			<tbody>
			<?php 
			$i = 0; 
			foreach($stats as $key):
			   $i++;
			?>
			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $activeButton<3 ? "<a id=$key->guid href=\"/Trains/?page=train&amp;guid=$key->guid\">$key->guid</a>" : "<a id=$key->guid href=\"/Tracks/?page=station&amp;station=$key->guid\">$key->guid</a>"; ?></td>
				<td><?php echo gmdate("H:i:s", $key->latenessAverage); ?></td>
				<td><?php echo gmdate("H:i:s", $key->latenessMax); ?></td>
			    <td><?php echo $key->speedAverage; ?></td>
				<td><?php echo $key->speedMax; ?></td>					
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		
        <a class="btn btn-large btn-success" href="/Hof/?stat=<?php echo $stat; ?>" >Alkuun</a>
		
      </div>
