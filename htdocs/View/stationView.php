<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->
      <div class="jumbotron">
        <h2><?php echo $station ?></h2>
		
        <p class="lead">
		<div class="navbar">
          <div class="navbar-inner">
            <div class="container">
			<form class="navbar-form pull-center disabledateboxes">
			<input type="hidden" name="page" value="station">
			<input type="hidden" name="station" value="<?php echo $station; ?>">
			<select id="time" name="time" title="Aika jolta tietoja haetaan.">
				<option value="all" <?php echo $time!="month" && $time!="week" && $time!="recent" ? "selected" : '' ?>>Kaikki</option>
				<option value="recent" <?php echo $time=="recent" ? "selected" : '' ?>>Viimeaikaiset</option>
				<option value="month" <?php echo $time=="month" ? "selected" : '' ?>>Kuukausi</option>
				<option value="week" <?php echo $time=="week" ? "selected" : '' ?>>Viikko</option>
			</select>
			<input type="text" id="number" title="Viikon (1-53) tai kuukauden (1-12) numero."  name="number" value="<?php echo $number; ?>">
			<input type="text" id="year" title="Vuosi" name="year" value="<?php echo $year; ?>">
			<input type="submit" value="Hae">
			</form>
            </div>
          </div>
        </div>
		<h3>lähtevät</h3>
		<table border=1  class="table table-hover tabletrains adddateparams tablesorter addSortparams">
			<thead>
			<tr class="tabletitlerow">
				<th>Pääteasema</th>
				<th>Myöhässä avg</th>
				<th>Myöhässä max</th>
				<th>Keskinopeus</th>
				<th>Nopeus max</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($stats as $key): 
				if($key->latenessAverage == 0 && $key->latenessMax == 0 && $key->speedAverage == 0 && $key->speedMax == 0)
				{
					continue;
				}
			?>
			<tr>
				<td><?php echo "<a id=$key->nextStation href=\"/Tracks/?page=station&amp;station=$key->nextStation\">$key->nextStation</a>"; ?></td>
				<td><?php echo gmdate("H:i:s", $key->latenessAverage); ?></td>
				<td><?php echo gmdate("H:i:s", $key->latenessMax); ?></td>
				<td><?php echo $key->speedAverage; ?></td>
				<td><?php echo $key->speedMax; ?></td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<h3>saapuvat</h3>
		<table border=1  class="table table-hover tabletrains adddateparams tablesorter addSortparams">
			<thead>
			<tr class="tabletitlerow">
				<th>Lähtöasema</th>
				<th>Myöhässä avg</th>
				<th>Myöhässä max</th>
				<th>Keskinopeus</th>
				<th>Nopeus max</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($statsar as $key): 
				if($key->latenessAverage == 0 && $key->latenessMax == 0 && $key->speedAverage == 0 && $key->speedMax == 0)
				{
					continue;
				}
			?>
			<tr>
				<td><?php echo "<a id=$key->previousStation href=\"/Tracks/?page=station&amp;station=$key->previousStation\">$key->previousStation</a>"; ?></td>
				<td><?php echo gmdate("H:i:s", $key->latenessAverage); ?></td>
				<td><?php echo gmdate("H:i:s", $key->latenessMax); ?></td>
				<td><?php echo $key->speedAverage; ?></td>
				<td><?php echo $key->speedMax; ?></td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		</p>
      </div>