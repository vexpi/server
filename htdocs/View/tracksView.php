<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->
      <div class="jumbotron">
        <h2>Täällä näet missä junat kulkevat</h2>
			<form action="moveMarker" class="navbar-form pull-center">
				<input type="text" id="lat" name="latitude" value="">
				<input type="text" id="lng" name="longitude" value="">
				<button type="button" onclick="toNewLocation()">Go</button>
				<input type="text" id="kmlUrl" name="kmlUrl" value="">
				<button type="button" onclick="setKmlLayer()">Get kml</button>
			</form>
			
			<div id="map-canvas"></div>
        <a class="btn btn-large btn-success" href="/index.php" >Alkuun</a>
      </div>
	  