<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->
      <div class="jumbotron">
        <h2>Juna <?php echo $guid; ?></h2>
		<div class="navbar withoutMargin">
          <div class="navbar-inner">
            <div class="container">
			<form class="navbar-form pull-center disabledateboxes">
			<input type="hidden" name="page" value="train">
			<input type="hidden" name="guid" value="<?php echo $guid; ?>">
			<select id="time" name="time" title="Aika jolta tietoja haetaan."><!-- onChange="selchanged()"-->
				<option value="all" <?php echo $time!="month" && $time!="week" && $time!="recent" ? "selected" : '' ?>>Kaikki</option>
				<option value="recent" <?php echo $time=="recent" ? "selected" : '' ?>>Viimeaikaiset</option>
				<option value="month" <?php echo $time=="month" ? "selected" : '' ?>>Kuukausi</option>
				<option value="week" <?php echo $time=="week" ? "selected" : '' ?>>Viikko</option>
			</select>
			<input type="text" id="number" name="number" title="Viikon (1-53) tai kuukauden (1-12) numero." value="<?php echo $number; ?>">
			<input type="text" id="year" name="year" title="Vuosi" value="<?php echo $year; ?>">
			<input type="submit" value="Hae">
			</form>
            </div>
          </div>
        </div>
		<table id="junataulu" border=1 class="table table-hover tabletrains tablesorter addSortparams">
			<thead>
			<tr class="tabletitlerow">
				<th class="header">Lähtöasema</th>
				<th class="header">Pääteasema</th>
				<th class="header">Myöhässä avg</th>
				<th class="header">Myöhässä max</th>
				<th class="header">Keskinopeus</th>
				<th class="header">Nopeus max</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($train as $key): ?>
			<tr>
				<td><?php echo $key->previousStation; ?></td>
				<td><?php echo $key->nextStation; ?></td>
				<td><?php echo gmdate("H:i:s", $key->latenessAverage); ?></td>
				<td><?php echo gmdate("H:i:s", $key->latenessMax); ?></td>
				<td><?php echo $key->speedAverage; ?></td>
				<td><?php echo $key->speedMax; ?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
		</table>
		
		<a class="btn btn-large btn-success" href="<?php echo "/Trains/?page=trains&amp;time=" . $time . "&amp;number=" . $number . "&amp;year=" . $year . "#" . $guid; ?>">Takaisin</a>
      </div>