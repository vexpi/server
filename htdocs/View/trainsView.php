<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->      
	  <div class="jumbotron">
        <h2><?php
		switch ($trainCat) {
		case "AE":
			echo "Allegro";
			break;
		case "P":
			echo "Pikajuna";
			break;
		case "H":
			echo "Henkilöjuna";
			break;
		case "IC":
			echo "Intercity";
			break;
		case "S":
			echo "Pendolino";
			break;
		default:
			echo "Kaikki Junat";
}
?></h2>
		<p class="lead">
		<div class="navbar withoutMargin">
          <div class="navbar-inner">
            <div class="container">
			<form method="post" class="navbar-form pull-center disabledateboxes">
			<select id="time" name="time" title="Aika jolta tietoja haetaan.">
				<option value="all" <?php echo $time!="month" && $time!="week" && $time!="recent" ? "selected" : '' ?>>Kaikki</option>
				<option value="recent" <?php echo $time=="recent" ? "selected" : '' ?>>Viimeaikaiset</option>
				<option value="month" <?php echo $time=="month" ? "selected" : '' ?>>Kuukausi</option>
				<option value="week" <?php echo $time=="week" ? "selected" : '' ?>>Viikko</option>
			</select>
			<input type="text" id="number" title="Viikon (1-53) tai kuukauden (1-12) numero."  name="number" value="<?php echo $number; ?>">
			<input type="text" id="year" title="Vuosi" name="year" value="<?php echo $year; ?>">
			</form>
              <ul class="nav adddateparams">
				<li <?php if($trainCat == '') echo 'class="active"'; ?>><a href="/Trains/?page=trains">Kaikki Junat</a></li>
				<li <?php if($trainCat == 'AE') echo 'class="active"'; ?>><a href="/Trains/?page=trains&amp;cat=AE">Allegro</a></li>
				<li <?php if($trainCat == 'P') echo 'class="active"'; ?>><a href="/Trains/?page=trains&amp;cat=P">Pikajuna</a></li>
				<li <?php if($trainCat == 'H') echo 'class="active"'; ?>><a href="/Trains/?page=trains&amp;cat=H">Henkilöjuna</a></li>
				<li <?php if($trainCat == 'IC') echo 'class="active"'; ?>><a href="/Trains/?page=trains&amp;cat=IC">InterCity</a></li>
                <li <?php if($trainCat == 'S') echo 'class="active"'; ?>><a href="/Trains/?page=trains&amp;cat=S">Pendolino</a></li> 
              </ul>
            </div>
          </div>
        </div>

		<table border=1  class="table table-hover tabletrains adddateparams tablesorter addSortparams">
			<thead>
			<tr class="tabletitlerow">
				<th>Juna</th>
				<th>Myöhässä avg</th>
				<th>Myöhässä max</th>
				<th>Keskinopeus</th>
				<th>Nopeus max</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($stats as $key): 
				if($key->latenessAverage == 0 && $key->latenessMax == 0 && $key->speedAverage == 0 && $key->speedMax == 0)
				{
					continue;
				}
			?>
			<tr>
				<td><?php echo "<a id=$key->guid href=\"/Trains/?page=train&amp;guid=$key->guid\">$key->guid</a>"; ?></td>
				<td <?php timeToColor($key->latenessAverage); ?> ><?php echo gmdate("H:i:s", $key->latenessAverage); ?></td>
				<td <?php timeToColor($key->latenessMax); ?> ><?php echo gmdate("H:i:s", $key->latenessMax); ?></td>
				<td><?php echo $key->speedAverage; ?></td>
				<td><?php echo $key->speedMax; ?></td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		
        <a class="btn btn-large btn-success" href="/Trains/?page=trains&amp;cat=<?php echo $trainCat; ?>" >Alkuun</a>
		
      </div>
<?php	  
function timeToColor($time)
{
	$timemax=1800.0;
	if ($time > $timemax) $time=$timemax;
	$frac = $time/$timemax;
	$minc = 0x80;
	$maxc = 0xf0;
	$r = floor($frac * ($maxc-$minc)) + $minc;
	$g = 0xFF-$r;
	echo "bgcolor=#30".dechex($r) .dechex($r);
	/*
	#TODO: järkevä väritys
	if ($time >350) echo "bgcolor=#FF5050";
	else if($time > 180) echo "bgcolor=#FF9999";
	else if($time > 120) echo "bgcolor=#99FF99";
	else if($time > 60) echo "bgcolor=#99FF99";
	else echo "bgcolor=#99FF99";
	*/
}
?>
