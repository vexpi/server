<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->
<div class="row">
  <div class="span12">
    <h1>Tervetuloa vexpiin !</h1>
    <div class="row">
      <div class="span6">
	<form action="Trains/" class='form-horizontal'>
		<input type="hidden" id="page" name="page" value="train">
		<legend>Junahaku</legend>
		<div class="row-fluid">
		<table border="0"><tr>
			<td align="center" width="33%">
				<label for="guid" align="center" valign="center">Juna</label>
			</td>
			<td  align="center" width="33%">
				<input type="text" id="guid" name="guid" class="input-block-level" title="Junatunnus, esimerkiksi P123." required>
			</td>
			<td  align="center" width="33%">
				<button type="submit" class="btn btn-primary btn-large">Hae</button>
			</td>
		</tr></table>
		</div>
	</form>
	  </div>
      <div class="span6">
	    <form action="Tracks/" class='form-horizontal'>
		<input type="hidden" id="page" name="page" value="station">
		<fieldset>
			<legend>Asemahaku</legend>
			<div class="row-fluid">
		<table border="0"><tr>
			<td align="center" width="33%">
				<label for="station" align="center" valign="center">Asema</label>
			</td>
			<td  align="center" width="33%">
				<input type="text" id="station" name="station" class="input-block-level" title="Aseman nimi, esimerkiksi Jyväskylä." required>
			</td>
			<td  align="center" width="33%">
				<button type="submit" class="btn btn-primary btn-large">Hae</button>
			</td>
		</tr></table>
		</div>
		</form>
	  </div>
    </div>
  </div>
</div>