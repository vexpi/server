<!--Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html -->
<div class="masthead">
        <h3 class="muted">vexpi -projekti</h3>
        <div class="navbar">
          <div class="navbar-inner">
            <div class="container">
              <ul class="nav">
				
                <li <?php if($active == 'home') echo 'class="active"'; ?>><a href="index.php">Pääsivu</a></li> <!--class="active"  // tänne saisi miettiä jonkin hyvän tavan php:llä näyttää aktiivisen sivun, voin kyllä itekin sen tehdä --> 
                <li <?php if($active == 'trains') echo 'class="active"';?>><a href="Trains/">Junat</a></li>
				<li <?php if($active == 'tracks') echo 'class="active"';?>><a href="Tracks/">Asemat</a></li>
				<li <?php if($active == 'hof') echo 'class="active"';?>><a href="Hof/">Hall of Fame</a></li>
              </ul>
            </div>
          </div>
        </div><!-- /.navbar -->
      </div>