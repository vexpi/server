/*Vexpi train tracking software
Copyright (C) 2013  Aissa Baccouche Samir, Hakavuori Eero, Heikkilä Oula, Junttila Jere

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/agpl-3.0.html */

Date.prototype.getWeek = function () {  
    var target  = new Date(this.valueOf());
    var dayNr   = (this.getDay() + 6) % 7;
    target.setDate(target.getDate() - dayNr + 3);
    var firstThursday = target.valueOf();
    target.setMonth(0, 1);
    if (target.getDay() != 4) {  
        target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);  
    }
    return 1 + Math.ceil((firstThursday - target) / 604800000); // 604800000 = 7 * 24 * 3600 * 1000  
}
function updateDateBoxes(){
			var form = $('.disabledateboxes');
			var i = form.find('#time').prop('selectedIndex');
			var number = form.find('#number')
			var year = form.find('#year')
			var disabled = i<2;
			number.prop('disabled',disabled);
			year.prop('disabled',disabled);
			switch(i)
			{
				case 2:
					var d = new Date();
					number.prop('value',d.getMonth()+1);
					year.prop('value',d.getFullYear());
					break;
				case 3:
					var d = new Date();
					number.prop('value',d.getWeek());
					year.prop('value',d.getFullYear());
					break;
				default:
					number.prop('value','');
					year.prop('value','');
			}
		}
$(document).ready(function(){	
	$('.addSortparams').tablesorter();
	$('.adddateparams').find("a").click(function(event){
		event.preventDefault();
		var number = $('#number').attr('value'); 
		var year = $('#year').attr('value');
		var time = $("#time").find("option:selected").attr('value') ;
		stringToSend = "&number="+number+"&year="+year+"&time="+time;
		window.location.href = $(this).attr('href') + stringToSend;
	});
	
	$('.disabledateboxes').find("select").change(updateDateBoxes);
	var form = $('.disabledateboxes');
	var disabled = form.find('#time').prop('selectedIndex') < 2;
	form.find('#number').prop('disabled',disabled);
	form.find('#year').prop('disabled',disabled);
});